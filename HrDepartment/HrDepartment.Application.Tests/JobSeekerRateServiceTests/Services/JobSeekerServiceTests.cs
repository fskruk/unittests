﻿using NUnit.Framework;
using HrDepartment.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using HrDepartment.Domain.Entities;
using HrDepartment.Domain.Enums;
using Moq;
using HrDepartment.Infrastructure.Interfaces;
using HrDepartment.Application.Interfaces;
using AutoMapper;

namespace HrDepartment.Application.Services.Tests
{
    [TestFixture()]
    public class JobSeekerServiceTests
    {
        JobSeeker seeker = new JobSeeker();
        Mock<IStorage> storage = new Mock<IStorage>();
        Mock<IRateService<JobSeeker>> rateService = new Mock<IRateService<JobSeeker>>();        
        IMapper mapper = Mock.Of<IMapper>();
        INotificationService notificationService = Mock.Of<INotificationService>();

        private JobSeekerService GetJobSeekerService()
        {
            return new JobSeekerService(storage.Object, rateService.Object, mapper, notificationService);
        }

        [Test()]
        [TestCase(15)]
        [TestCase(100)]
        public void RateJobSeekerAsyncTest(int expectedRate)
        {
            storage.Setup(e => e.GetByIdAsync<JobSeeker, int>(It.IsAny<int>())).ReturnsAsync(seeker);
            rateService.Setup(e => e.CalculateJobSeekerRatingAsync(seeker)).ReturnsAsync(expectedRate);
            var jobSeekerService = GetJobSeekerService();

            var resultRate = jobSeekerService.RateJobSeekerAsync(seeker.Id);

            Assert.AreEqual(expectedRate, resultRate.Result);
        }
    }
}
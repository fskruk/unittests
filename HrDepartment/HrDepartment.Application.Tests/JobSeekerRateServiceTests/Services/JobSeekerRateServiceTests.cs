﻿using NUnit.Framework;
using HrDepartment.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using HrDepartment.Domain.Entities;
using Moq;
using HrDepartment.Application.Interfaces;
using System.Threading.Tasks;
using HrDepartment.Domain.Enums;

namespace HrDepartment.Application.Services.Tests
{
    [TestFixture()]
    public class JobSeekerRateServiceTests
    {
        JobSeeker seeker;        

        private JobSeeker GetJobSeeker()
        {
            seeker = new JobSeeker()
            {
                FirstName = "A",
                MiddleName = "B",
                LastName = "C",
                Education = EducationLevel.None,
                BadHabits = BadHabits.None,
                Experience = 0,
                BirthDate = DateTime.Now
            };
            return seeker;
        }

        private JobSeekerRateService GetJobSeekerRateService(bool inList)
        {           
            return new JobSeekerRateService(GetISanctionService(inList));
        }

        private JobSeekerRateService GetJobSeekerRateService()
        {
            return new JobSeekerRateService(GetISanctionService(false));
        }

        private ISanctionService GetISanctionService(bool inList)
        {
            ISanctionService sanctionService = Mock.Of<ISanctionService>(
            e => e.IsInSanctionsListAsync
            (
                seeker.LastName,
                seeker.FirstName,
                seeker.MiddleName,
                seeker.BirthDate
            ) == Task.Run(() => inList));
            return sanctionService;
        }

        [Test()]
        [TestCase( 0,  15)]
        [TestCase(18, 25)]
        [TestCase(30, 25)]
        [TestCase(64, 25)]
        [TestCase(65, 15)]
        [TestCase(100, 15)]
        public void CalculateBirthDateRatingTest_ReturnCorrectRating(int offsetYear, int expectedRating)
        {            
            var seeker = GetJobSeeker();
            var jobSeekerRateService = GetJobSeekerRateService();
            seeker.BirthDate = seeker.BirthDate.AddYears(-offsetYear);

            var actualRating = jobSeekerRateService.CalculateJobSeekerRatingAsync(seeker);

            Assert.AreEqual(expectedRating, actualRating.Result);
        }

        [TestCase(EducationLevel.None, 15)]
        [TestCase(EducationLevel.School, 20)]
        [TestCase(EducationLevel.College, 30)]
        [TestCase(EducationLevel.University, 50)]
        
        public void CalculateEducationRating_ReturnCorrectRating(EducationLevel educationLevel, int expectedRating)
        {
            var seeker = GetJobSeeker();
            var jobSeekerRateService = GetJobSeekerRateService();
            seeker.Education = educationLevel;

            var actualRating = jobSeekerRateService.CalculateJobSeekerRatingAsync(seeker);

            Assert.AreEqual(expectedRating, actualRating.Result);
        }

        [TestCase(10)]
        [TestCase(-1)]
        public void CalculateEducationRating_UnexpectedEducation_ReturnArgumentOutOfRangeException(EducationLevel educationLevel)
        {
            var seeker = GetJobSeeker();
            var jobSeekerRateService = GetJobSeekerRateService();
            seeker.Education = educationLevel;

            Assert.ThrowsAsync<ArgumentOutOfRangeException>(() =>
               jobSeekerRateService.CalculateJobSeekerRatingAsync(seeker)
                );
        }


        [TestCase(false, 15)]
        [TestCase(true, 0)]

        public void IsInSanctionList_bool_ReturnCorrectRating(bool inList, int expectedRating)
        {
            var seeker = GetJobSeeker();
            var jobSeekerRateService = GetJobSeekerRateService(inList);

            var actualRating = jobSeekerRateService.CalculateJobSeekerRatingAsync(seeker);

            Assert.AreEqual(expectedRating, actualRating.Result);
        }

        [TestCase(0, 15)]
        [TestCase(1, 20)]
        [TestCase(2, 20)]
        [TestCase(3, 25)]
        [TestCase(5, 35)]
        [TestCase(6, 35)]
        [TestCase(10, 50)]
        public void CalculateExperienceRatingTest_ReturnCorrectRating(int workExperienceInYears, int expectedRating)
        {
            var seeker = GetJobSeeker();
            var jobSeekerRateService = GetJobSeekerRateService();
            seeker.Experience += workExperienceInYears;

            var actualRating = jobSeekerRateService.CalculateJobSeekerRatingAsync(seeker);

            Assert.AreEqual(expectedRating, actualRating.Result);
        }

        [TestCase(BadHabits.None, 15)]
        [TestCase(BadHabits.Smoking, 10)]
        [TestCase(BadHabits.Alcoholism, 0)]
        [TestCase(BadHabits.Drugs, 0)]
        public void CalculateHabitsRatingTest_ReturnCorrectRating(BadHabits bh, int expectedRating)
        {
            var seeker = GetJobSeeker();
            var jobSeekerRateService = GetJobSeekerRateService();
            seeker.BadHabits = bh;

            var actualRating = jobSeekerRateService.CalculateJobSeekerRatingAsync(seeker);

            Assert.AreEqual(expectedRating, actualRating.Result);
        }
    }
}